package models

type Document struct {
	DateAdded             string        `json:"date_added"`
	Description           string        `json:"description"`
	DocumentType          DocumentType  `json:"document_type"`
	DocumentTypeChangeUrl string        `json:"document_type_change_url"`
	Id                    int           `json:"id"`
	Label                 string        `json:"label"`
	Language              string        `json:"language"`
	LatestVersion         LatestVersion `json:"latest_version"`
	Url                   string        `json:"url"`
	Uuid                  string        `json:"uuid"`
	Pk                    int           `json:"pk"`
	VersionsUrl           string        `json:"versions_url"`
}

type NewDocument struct {
	Description  string `json:"description"`
	DocumentType int    `json:"document_type"`
	Id           int    `json:"id"`
	File         string `json:"file"`
	Label        string `json:"label"`
	Language     string `json:"language"`
}

type DocumentType struct {
	DeleteTimePeriod int    `json:"delete_time_period"`
	DeleteTimeUnit   string `json:"delete_time_unit"`
	DocumentsUrl     string `json:"documents_url"`
	DocumentsCount   int    `json:"documents_count"`
	Id               int    `json:"id"`
	Label            string `json:"label"`
	TrashTimePeriod  int    `json:"trash_time_period"`
	TrashTimeUnit    string `json:"trash_time_unit"`
	Url              string `json:"url"`
	Filenames        []struct {
		Filename string `json:"filename"`
	} `json:"filenames"`
}

type LatestVersion struct {
	Checksum    string `json:"checksum"`
	Comment     string `json:"comment"`
	DocumentUrl string `json:"document_url"`
	DownloadUrl string `json:"download_url"`
	Encoding    string `json:"encoding"`
	File        string `json:"file"`
	Mimetype    string `json:"mimetype"`
	PagesUrl    string `json:"pages_url"`
	Size        int    `json:"size"`
	Timestamp   string `json:"timestamp"`
	Url         string `json:"url"`
}
