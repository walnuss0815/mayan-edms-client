package client

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/go-resty/resty/v2"
	"gitlab.com/walnuss0815/mayan-edms-client/models"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type Client struct {
	Url    string
	Client *resty.Client
	https  bool
}

type PaginatedResult struct {
	Count   int           `json:"count"`
	Next    string        `json:"next"`
	Results []interface{} `json:"results"`
}

// Create new Mayan EDMS client
func NewClient(url string, username string, password string, ignoreTlsError bool) *Client {
	c := new(Client)
	c.Url = url

	c.Client = resty.New()
	c.Client.SetBasicAuth(username, password)

	if ignoreTlsError {
		c.Client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	}

	// Workaround: API scheme
	if strings.HasPrefix(c.Url, "https://") {
		c.https = true
	} else {
		c.https = false
	}

	return c
}

// Create document with given document type id and upload the given file
func (c Client) CreateDocument(filePath string, typeId int) (*models.NewDocument, error) {
	fileBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	fileName := filepath.Base(filePath)

	f, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	// Get the content
	contentType, err := GetFileContentType(f)
	if err != nil {
		panic(err)
	}

	response, err := c.Client.R().
		SetHeader("Accept", "application/json").
		SetMultipartField("file", fileName, contentType, bytes.NewReader(fileBytes)).
		SetFormData(map[string]string{
			"document_type": strconv.Itoa(typeId),
		}).
		SetResult(&models.NewDocument{}).
		Post(c.Url + "/api/documents/")

	if err != nil {
		return nil, err
	}

	if response.IsError() {
		return nil, fmt.Errorf("unable create document: %s", response.Status())
	}

	document := response.Result().(*models.NewDocument)

	return document, nil
}

// Get documents from Mayan EDMS
func (c Client) GetDocuments() ([]models.Document, error) {
	nextUrl := c.Url + "/api/documents/"

	documents := make([]models.Document, 0)

	for nextUrl != "" {
		response, err := c.Client.R().
			SetHeader("Accept", "application/json").
			SetResult(PaginatedResult{}).
			Get(nextUrl)

		if err != nil {
			return nil, err
		}

		if response.IsError() {
			return nil, fmt.Errorf("unable fetch documents: %s", response.Status())
		}

		documentResponse := response.Result().(*PaginatedResult)

		for _, r := range documentResponse.Results {
			document := models.Document{}

			jsonString, err := json.Marshal(r)
			if err != nil {
				return nil, err
			}

			err = json.Unmarshal(jsonString, &document)
			if err != nil {
				return nil, err
			}

			documents = append(documents, document)
		}

		nextUrl = c.fixScheme(documentResponse.Next)
	}

	return documents, nil
}

// Get document types from Mayan EDMS
func (c Client) GetDocumentTypes() ([]models.DocumentType, error) {
	nextUrl := c.Url + "/api/document_types/"

	documentTypes := make([]models.DocumentType, 0)

	for nextUrl != "" {
		response, err := c.Client.R().
			SetHeader("Accept", "application/json").
			SetResult(PaginatedResult{}).
			Get(nextUrl)

		if err != nil {
			return nil, err
		}

		if response.IsError() {
			return nil, fmt.Errorf("unable fetch document types: %s", response.Status())
		}

		documentTypeResponse := response.Result().(*PaginatedResult)

		for _, r := range documentTypeResponse.Results {
			documentType := models.DocumentType{}

			jsonString, err := json.Marshal(r)
			if err != nil {
				return nil, err
			}

			err = json.Unmarshal(jsonString, &documentType)
			if err != nil {
				return nil, err
			}

			documentTypes = append(documentTypes, documentType)
		}

		nextUrl = c.fixScheme(documentTypeResponse.Next)
	}

	return documentTypes, nil
}

// Get document by given document ID from Mayan EDMS
func (c Client) GetDocumentById(documentId int) (*models.Document, error) {
	response, err := c.Client.R().
		SetHeader("Accept", "application/json").
		SetResult(models.Document{}).
		SetPathParams(map[string]string{
			"documentId": strconv.Itoa(documentId),
		}).
		Get("/api/document/{documentId}")

	if err != nil {
		return nil, err
	}

	if response.IsError() {
		return nil, fmt.Errorf("unable fetch document types: %s", response.Status())
	}

	document := response.Result().(*models.Document)

	return document, nil
}

// Get documents from Mayan EDMS
func (c Client) DownloadVersion(version models.LatestVersion, filePath string) error {
	response, err := c.Client.R().
		SetOutput(filePath).
		Get(c.fixScheme(version.DownloadUrl))

	if err != nil {
		return err
	}

	if response.IsError() {
		return fmt.Errorf("unable download document version: %s", response.Status())
	}
	return nil
}

func (c Client) fixScheme(url string) string {
	if c.https {
		return strings.Replace(url, "http://", "https://", -1)
	} else {
		return url
	}
}
